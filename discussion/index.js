//console.log("Hello World!");

//ES6 Updates
//ES6 is one of the latest versions of writing Javascript and in fact is one of the latest major update to JS.
//let,const - are ES6 updates are the new standards of creating variables
//var - was the keyword to create variables before ES6;

//console.log(varSample);
//var varSample = "Hoist me up!";

//Exponent Operator

let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);
//Math.pow() allows us to get the result of a number raised to a given exponent.
//Math.pow(base,exponent)

//Exponent Operators ** - allows us to get the result of a number raised to a given exponent. It is used as an alternative to Math.pow();
let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);//25

let fivePowerOf4 = 5**4;
console.log(fivePowerOf4);//625

let squareRootOf4 = 4**.5;
console.log(squareRootOf4);//2

let string1 = "Javascript";
let string2 = "not";
let string3 = "is";
let string4 = "Typescript";
let string5 = "Java";
let string6 = "Zuitt";
let string7 = "Coding";

/*
	Mini-Activity

	1. Create new variables called sentence1 and sentence2
	2. Concatenate and save the resulting string into sentence1:
		"Javascript is not Java"
	3. Concatenate and save the resulting string into sentence2:
		"Typescript is Javascript"
	
	Log both variables in the console and take a screenshot.

	The sentences must have spaces.

*/

// let sentence1 = string1 + " " + string3 + " " + string2 + " " + string5;
// let sentence2 = string4 + " " + string3 + " " + string1;

// console.log(sentence1);
// console.log(sentence2);

//Template Literals
//"",'' - string literals
//Template Literals allow us to create strings using `` and easily embed JS expression in it.

let sentence1 = `${string1} ${string3} ${string2} ${string5}`;
let sentence2 = `${string4} ${string3} ${string1}`;

console.log(sentence1);
console.log(sentence2);

/*
	${} is a placeholder it is used to embed JS expressions when creating strings using Template Literals.
*/

//Use template literals to add a new sentence in our sentence3 variable:
//Make sure to use the variables for Zuitt and Coding
//"Zuitt Coding Bootcamp"
let sentence3 = `${string6} ${string7} Bootcamp`;
console.log(sentence3);

//Template Literal with JS Expression
let sentence4 = `The sum of 15 and 25 is ${15+25}`;
console.log(sentence4);

let person = {
	name: "Michael",
	position: "developer",
	income: 50000,
	expenses: 60000
}

console.log(`${person.name} is a ${person.position}`);
console.log(`His income is ${person.income} and expenses at ${person.expenses}. His current balance is ${person.income - person.expenses}`)

//Destructuring Arrays and Objects
//Destructuring will allow us to save Array items or Object properties into new variables without having to create/initialize with accessing the items/properties one by one

let array1 = ["Curry","Lillard","Paul","Irving"];

// let player1 = array1[0];//"Curry"
// let player2 = array1[1];//"Lillard"
// let player3 = array1[2];//"Paul"
// let player4 = array1[3];//"Irving"
// console.log(player1,player2,player3,player4);

//Array destructuring is when we save array items into variables.
//In arrays, order matters and that goes the same to our destructuring
let [player1,player2,player3,player4] = array1;

console.log(player1,player2,player3,player4);

let array2 = ["Jokic","Embiid","Howard","Anthony-Towns"];

//Get and save all items into variables except for Howard
let [center1,center2,,center4] = array2;

console.log(center4);

//Object Destructuring
//In object destructuring, the order of destructuring does not matter, however, the name of the variable must match a property in the object.

let pokemon1 = {

	name: "Bulbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf","Tackle","Leech Seed"]

}

//Object Destructuring
let {level,type,name,moves,personality} = pokemon1;

console.log(level);
console.log(type);
console.log(name);
console.log(moves);
console.log(personality);

let pokemon2 = {

	name: "Charmander",
	type: "Fire",
	level: 11,
	moves: ["Ember","Scratch"]

}

//{propertyName: newVariable}
const {name: name2} = pokemon2;
console.log(name2);

//Arrow Functions
//Arrow Function are an alternative way of writing functions in JS. However, there are significant pros and cons between traditional and arrow functions

//Traditional Function
function displayMsg(){
	console.log(`Hello, World!`);
}

displayMsg();

//Arrow Function
const hello = () => {
	console.log(`Hello From Arrow!`);
}

hello();

//Arrow Function with Parameters
const greet = (friend) => {

	//console.log(friend);
	console.log(`Hi! ${friend.name}`);

}

greet(person);


//Arrow vs Traditional Function

//Implicit Return - allows us to return a value from an arrow function without the use of return keyword.

//traditional addNum() function

/*function addNum(num1,num2){

	//console.log(num1,num2);

	let result = num1+num2;
	return result

	return num1+num2;

}

let sum = addNum(5,10);
console.log(sum);*/

//Implicit return, we return values without the return keyword.

let subNum = (num1,num2) => num1 - num2;

let difference = subNum(10,5);
console.log(difference);

//Implicit return will only work on arrow functions without {}
//{} in arrow functions/functions are code blocks. If an arrow function has a {} or code block, we're going to need to use a return.


/*
	Mini-Activity

	Translate our previous addNum traditional function into an arrow function with an implicit return

*/

const addNum = (num1,num2) => num1 + num2;

let sum = addNum(50,70);
console.log(sum)

//Traditional functions vs Arrow Function as Methods

let character1 = {

	name: "Cloud Strife",
	occupation: "SOLDIER",
	greet: function(){

		//In a traditional function as a method:
		//this keyword refers to the current object where the method is.
		//console.log(this);
		console.log(`Hi! I'm ${this.name}`);

	},
	introduceJob: () => {
		//In an arrow function as method,
		//The this keyword will NOT refer to the current object. Instead, it will refer to the global window object.
		console.log(this);
	}

}

character1.greet();
character1.introduceJob();

//Class Based Objects Blueprints
	//In Javascript, Classes are templates of objects.
	//We can create objects out of the use of Classes.
	//Before the introduction of Classes in JS, we mimic this behavior of being able to create objects of the same blueprint using constructor function.

	//Constructor Function
	// function Pokemon(name,type,level){

	// 	this.name = name;
	// 	this.type = type;
	// 	this.level = level;

	// }

	//With the advent of ES6, we are now introduced to a special method of creating and initializing an object.
	//Classes- PascalCase
	//Normal Func - camelCase

	class Car {
		constructor(brand,name,year){

			this.brand = brand;
			this.name = name;
			this.year = year;

		}
	}

	let car1 = new Car("Toyota","Vios","2002");
	let car2 = new Car("Cooper","Mini","1969");
	let car3 = new Car("Porsche","911","1967");


	console.log(car1);
	console.log(car2);
	console.log(car3);


/*
	Mini-Activity:

	- Translate the Pokemon constructor function as a Class Constructor 
	- Create 2 new pokemons out of the class contructor and save it in their respective variables.
	- Log the values in the console.

*/

 class Pokemon {

 	constructor(name,type,level){

 		this.name = name;
 		this.type = type;
 		this.level = level;

 	}

 }

 let pokemon3 = new Pokemon("Groudon","Fire",65);
 let pokemon4 = new Pokemon("Kyogre","Water",60);

 console.log(pokemon3);
 console.log(pokemon4);
